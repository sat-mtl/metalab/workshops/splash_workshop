#!/bin/bash

sudo apt install -y git
mkdir -p ${HOME}/src
cd ${HOME}/src

# Install Splash
git clone https://gitlab.com/sat-metalab/splash
cd splash
sudo apt install -y build-essential git-core cmake libxrandr-dev libxi-dev \
    mesa-common-dev libgsl0-dev libatlas3-base libgphoto2-dev libz-dev \
    libxinerama-dev libxcursor-dev python3-dev yasm portaudio19-dev \
    python3-numpy libopencv-dev libjsoncpp-dev libx264-dev \
    libx265-dev
./make_deps.sh
mkdir build && cd build
cmake ..
make -j$(nproc) && sudo make install

# Install blender
sudo add-apt-repository ppa:thomas-schiex/blender
sudo apt install -y blender
